#!/usr/bin/env ruby

require 'fileutils'
require 'hashie'
require 'yaml'

SPLIT_SIZE = 100

class Definitions < Hash
  include Hashie::Extensions::DeepMerge
end

swagger_object = YAML.load_file('openapi_v2.yml')
gitlab_ci = { 
  'stages' => [
    'fuzz'
  ],
  'include' => [
    'template' => 'API-Fuzzing.gitlab-ci.yml'
  ],
  'variables' => {
    'FUZZAPI_PROFILE' => 'Medium-50',
    'FUZZAPI_OVERRIDES_ENV' => '{"headers":{"PRIVATE-TOKEN":"$PERSONAL_ACCESS_TOKEN"}}',
    'FUZZAPI_OPENAPI_RELAXED_VALIDATION' => 'On',
    'FUZZAPI_EXCLUDE_PATHS' => '/api/v4/personal_access_tokens/self'
  },
  'apifuzzer_fuzz' => {
    'rules' => [
      {
        'if' => '$CI_COMMIT_BRANCH',
        'when' => 'never'
      }
    ]
  },
}

FileUtils.mkdir('openapi_parts')

def extract_definition_from_ref(ref)
  ref.split('/').last
end

def extract_definitions_from_object(object, source_definitions)
  object.extend Hashie::Extensions::DeepFind
  if refs = object.deep_find_all('$ref')
    refs = refs.flatten.uniq
    definitions = refs.map { |ref| extract_definition_from_ref(ref) }
    extracted_definitions = Definitions[source_definitions.select { |k, _v| definitions.include?(k) }]
    extracted_definitions = extracted_definitions.deep_merge(extract_definitions_from_object(extracted_definitions,
                                                                                             source_definitions))
  end
  extracted_definitions.to_h
end

def extract_tags(paths_object, source_tags)
  paths_object.extend Hashie::Extensions::DeepFind
  tags = paths_object.deep_find_all('tags').flatten.uniq || []
  source_tags.select { |v| tags.include?(v['name']) }
end

def generate_job(swagger_object, swagger_object_part, gitlab_ci, index)
  swagger_object_part['definitions'] = extract_definitions_from_object(swagger_object_part, swagger_object['definitions'])
  swagger_object_part['tags'] = extract_tags(swagger_object_part['paths'], swagger_object['tags'])
  gitlab_ci["apifuzzer_fuzz_#{index}"] = {
    'extends' => 'apifuzzer_fuzz',
    'allow_failure' => true,
    'tags' => ['large-runner'],
    'variables' => {
      'FUZZAPI_OPENAPI' => "openapi_parts/openapi_#{index}.yml"
    },
    # needed to override when-never rule for apifuzzer_fuzz job 
    'rules' => [
      {
        'if' => '$CI_COMMIT_BRANCH'
      }
    ],
    'needs' => [
      {
        'pipeline' => '$PARENT_PIPELINE_ID',
        'job' => 'add-examples'
      }
    ]
  }
end

index = 0
method_count = 0
swagger_object_part = swagger_object.dup
swagger_object_part['definitions'] = {}
swagger_object_part['paths'] = {}
swagger_object['paths'].each do |path, path_object|
  path_object.each do |method, method_object|
    if method_count == SPLIT_SIZE
      generate_job(swagger_object, swagger_object_part, gitlab_ci, index)

      File.open("openapi_parts/openapi_#{index}.yml", 'w') { |f| f.write swagger_object_part.to_yaml }
      method_count = 0
      swagger_object_part['paths'] = {}
      index += 1
      next
    elsif swagger_object_part['paths'].key?(path)
      swagger_object_part['paths'][path][method] = method_object
    else
      swagger_object_part['paths'][path] = { method => method_object }
    end
    method_count += 1
  end
end

generate_job(swagger_object, swagger_object_part, gitlab_ci, index)
File.open("openapi_parts/openapi_#{index}.yml", 'w') { |f| f.write swagger_object_part.to_yaml }
File.open('generated-config.yml', 'w') { |f| f.write gitlab_ci.to_yaml }
