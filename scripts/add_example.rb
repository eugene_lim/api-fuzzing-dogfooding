#!/usr/bin/env ruby

require 'json'
require 'net/http'
require 'yaml'

def fetch_example_object_id(target_url, object_type)
  uri = URI("#{target_url}/api/v4/#{object_type}")
  req = Net::HTTP::Get.new(uri)
  req['PRIVATE-TOKEN'] = ENV['PERSONAL_ACCESS_TOKEN']

  res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') do |http|
    http.request(req)
  end
  JSON.parse(res.body)[0]['id']
end

# fetch example IDs
target_url = ENV['FUZZAPI_TARGET_URL']
example_group_id = fetch_example_object_id(target_url, 'groups')
example_project_id = fetch_example_object_id(target_url, 'projects')

# add example IDs
Dir.each_child('openapi_parts') do |filename|
  swagger_object = YAML.load_file("openapi_parts/#{filename}")
  swagger_object['paths'].each do |path, _path_object|
    next unless path.include?('groups/{id}') || path.include?('projects/{id}')

    swagger_object['paths'][path].each do |method, _method_object|
      # don't delete resources
      next if method == 'delete'

      id_parameter_index = swagger_object['paths'][path][method]['parameters'].index { |x| x['name'] == 'id' }
      swagger_object['paths'][path][method]['parameters'][id_parameter_index]['default'] =
        path.include?('groups/{id}') ? example_group_id : example_project_id
    end
  end
  File.open("openapi_parts/#{filename}", 'w') { |f| f.write swagger_object.to_yaml }
end
